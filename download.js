
    function showAddressPopup() {
        document.getElementById('address-popup').style.display = 'block';
    }

    function hideAddressPopup() {
        document.getElementById('address-popup').style.display = 'none';
    }

    // Close popup when clicking outside the content
    window.onclick = function(event) {
        const popup = document.getElementById('address-popup');
        if (event.target === popup) {
            hideAddressPopup();
        }
    };
    
   document.addEventListener("DOMContentLoaded", function() {
    // Add event listeners for each button
    document.getElementById('scrollToHomeBtn').addEventListener('click', () => {
        document.getElementById('home').scrollIntoView({ behavior: 'smooth' });
    });

    document.getElementById('scrollToProjectsBtn').addEventListener('click', () => {
        document.getElementById('projects').scrollIntoView({ behavior: 'smooth' });
    });

    document.getElementById('scrollToVideosBtn').addEventListener('click', () => {
        document.getElementById('videos').scrollIntoView({ behavior: 'smooth' });
    });

    document.getElementById('scrollToAbout').addEventListener('click', () => {
        document.getElementById('about').scrollIntoView({ behavior: 'smooth' });
    });

    document.getElementById('scrollToContact').addEventListener('click', () => {
        document.getElementById('contact').scrollIntoView({ behavior: 'smooth' });
    });

    document.getElementById('scrollToDownloadsBtn').addEventListener('click', () => {
        document.getElementById('downloads').scrollIntoView({ behavior: 'smooth' });
    });
});

	
	// Toggle platform packages visibility
function togglePlatform(platform) {
    // Hide all platforms
    const allPlatforms = document.querySelectorAll('.package-options');
    allPlatforms.forEach(function(item) {
        item.style.display = 'none';
    });

    // Show the selected platform
    const platformSection = document.getElementById(platform);
    platformSection.style.display = 'block';
}
function applyFilters() {
    const levelEditorFilter = document.getElementById('filter-level-editor').checked;
    const touchControlsFilter = document.getElementById('filter-touch-controls').checked;
    const vrFilter = document.getElementById('filter-vr').checked;
    const arcadeFilter = document.getElementById('filter-arcade').checked;
    const puzzleFilter = document.getElementById('filter-puzzle').checked;
    const platformFilter = document.getElementById('filter-platform').checked;  // Platform filter
    const multiplayerFilter = document.getElementById('filter-multiplayer').checked;  // Platform filter

    const games = document.querySelectorAll('.download-item');

    games.forEach(game => {
        const hasLevelEditor = game.getAttribute('data-level-editor') === 'true';
        const hasTouchControls = game.getAttribute('data-touch-controls') === 'true';
        const isArcade = game.getAttribute('data-arcade') === 'true';
        const isPuzzle = game.getAttribute('data-puzzle') === 'true';
        const isPlatform = game.getAttribute('data-platform') === 'true';  // Platform genre check
        const isMultiplayer = game.getAttribute('data-multiplayer') === 'true';  // Platform genre check

        const shouldShow = 
            (!levelEditorFilter || hasLevelEditor) &&
            (!touchControlsFilter || hasTouchControls) &&
            (!vrFilter || game.getAttribute('data-vr') === 'true') &&
            (!arcadeFilter || isArcade) &&
            (!puzzleFilter || isPuzzle) &&
            (!platformFilter || isPlatform) &&  // Apply the Platform filter
            (!multiplayerFilter || isMultiplayer);  // Apply the Platform filter

        game.style.display = shouldShow ? 'block' : 'none';
    });
}

// Function to open the modal with the game's expanded info
function openGameModal(game) {
    // Populate modal with the relevant data
    const modal = document.getElementById("game-modal");
    document.getElementById("modal-title").textContent = game.name;
    document.getElementById("modal-description").textContent = game.screenshots[0].description; // Set initial description
    document.getElementById("modal-repo").href = game.repoUrl;

    // Create additional screenshots dynamically (if applicable)
    const modalScreenshots = document.getElementById("modal-screenshots");
    modalScreenshots.innerHTML = ""; // Clear previous content
    game.screenshots.forEach(function (screenshot, index) {
        const imgContainer = document.createElement("div");
        imgContainer.classList.add("carousel-slide");
        if (index === 0) imgContainer.classList.add("active"); // Set first slide as active

        const img = document.createElement("img");
        img.src = screenshot.url;
        img.alt = game.name;
        imgContainer.appendChild(img);
        modalScreenshots.appendChild(imgContainer);
    });

    // Reset modal carousel state
    currentSlideModal = 0;
    showSlideModal(currentSlideModal);

    // Show the modal
    modal.style.display = "block";
}

// Close the modal when clicking on the close button
document.querySelector(".close-btn").addEventListener("click", function() {
    document.getElementById("game-modal").style.display = "none";
});

// Close modal if clicked outside the modal content
window.addEventListener("click", function(event) {
    if (event.target === document.getElementById("game-modal")) {
        document.getElementById("game-modal").style.display = "none";
    }
});

// Close the modal when clicking on the close button
document.querySelector(".close-btn").addEventListener("click", function() {
    document.getElementById("game-modal").style.display = "none";
});

// Close modal if clicked outside the modal content
window.addEventListener("click", function(event) {
    if (event.target === document.getElementById("game-modal")) {
        document.getElementById("game-modal").style.display = "none";
    }
});

let currentSlide = 0;
let currentSlideModal = 0;

// Function to show a slide in the main carousel
function showSlide(index) {
    const slides = document.querySelectorAll('.carousel-slide');
    if (index >= slides.length) currentSlide = 0;
    if (index < 0) currentSlide = slides.length - 1;
    slides.forEach((slide, i) => {
        slide.classList.toggle('active', i === currentSlide);
    });
}

// Function to show a slide in the modal carousel
function showSlideModal(index) {
    const modalSlides = document.querySelectorAll('#modal-screenshots .carousel-slide');
    if (index >= modalSlides.length) currentSlideModal = 0;
    if (index < 0) currentSlideModal = modalSlides.length - 1;
    modalSlides.forEach((slide, i) => {
        slide.classList.toggle('active', i === currentSlideModal);
    });
}

// Navigation for main carousel
function nextSlide() {
    currentSlide++;
    showSlide(currentSlide);
}

function prevSlide() {
    currentSlide--;
    showSlide(currentSlide);
}

// Navigation for modal carousel
function nextSlideModal() {
    currentSlideModal++;
    showSlideModal(currentSlideModal);
}

function prevSlideModal() {
    currentSlideModal--;
    showSlideModal(currentSlideModal);
}

// Function to check if the browser supports JXL
async function supportsJXL() {
    const img = new Image();
    img.src = "data:image/webp;base64,/"; // A minimal JXL image header
    try {
        await img.decode();
        return true;
    } catch (e) {
        return false;
    }
}

// Function to update image source based on support
async function updateScreenshotSources() {
    const webpSupported = await supportsJXL();

    document.querySelectorAll(".game-screenshot").forEach((img) => {
        let highResImage = img.getAttribute("data-highres");

        // If JXL is not supported, replace it with AVIF
        if (!webpSupported && highResImage.endsWith(".webp")) {
            img.setAttribute("data-highres", highResImage.replace(".webp", ".avif"));
        }
    });
}

// Run the function at the start
updateScreenshotSources();

// Initialize the first slide for both carousels
showSlide(currentSlide);
showSlideModal(currentSlideModal);

// Event listener for screenshot clicks to open the modal
document.querySelectorAll(".game-screenshot").forEach(function(img) {
    img.addEventListener("click", function() {
        const gameName = img.getAttribute("data-game");
        const highResImage = img.getAttribute("data-highres"); // Get the high-res version of the screenshot

        // Example data, you can modify this dynamically based on game data
        const gameData = {
   "Spinny the Runner": {
        name: "Spinny the Runner",
        description: "A platform game with rich and diverse mechanics built with the Boxclip engine, a 2D platformer engine, with an emphasis on interactive map editing.",
        repoUrl: "https://codeberg.org/glitchapp/spinny-the-runner",
        screenshots: [
            {
                url: "img/screenshots/spinny-highres.webp",
                description: "Explore vibrant levels full of animations and effects!"
            },
            {
                url: "img/screenshots/spinnyEditor.webp",
                description: "Level editor with tens of different entities, background, weather effects and hundred of assets"
            },
            {
                url: "img/screenshots/SpinnyMultiplayer.webp",
                description: "Multiplayer mode with split screen up to 4 players.."
            }
        ]
    },
            "Inertia Blast II": {
                name: "Inertia Blast II",
                description: "A fork of the remake of classic game Thrust II by PGimeno",
                repoUrl: "https://codeberg.org/glitchapp/inertia-blast-ii",
                 screenshots: [
            {
                url: "img/screenshots/inertiaii-highres.webp",
                description: "a perfect blend of retro charm and modern features"
            },
            {
                url: "img/screenshots/InertiaTouch.webp",
                description: "Mobile friendly touch controls"
            },
            {
                url: "img/screenshots/inertiaMenu.webp",
                description: "New splash and menus full of new options and features"
            },
            {
                url: "img/screenshots/InertiaAi.webp",
                description: "A.I. powered assistant with voice capabilities assist you with natural language "
            }
        ]
            },
            "Fish Fillets VR": {
                name: "Fish Fillets VR",
                description: "Port of fish fillets to VR. Beware: this game does not work on newest versions of LÖVR. Compatible LÖVR binaries included.",
                repoUrl: "https://codeberg.org/glitchapp/fishfillets-Vr",
               screenshots: [
            {
                url: "img/screenshots/ffvr-highres.webp",
                description: ""
            },
            {
                url: "img/screenshots/ffvr4.webp",
                description: ""
            },
            {
                url: "img/screenshots/ffvr3.webp",
                description: ""
            },
            {
                url: "img/screenshots/ffvr2.webp",
                description: ""
            }
        ]
            },
            
            "FFMini": {
    name: "FFMini",
    description: "Reduced version with core functionality and assets. For the full game, please download it from the repository",
    repoUrl: "https://codeberg.org/glitchapp/fish-fillets-remake",
    screenshots: [
      {
                url: "img/screenshots/ffmini-highres.webp",
                description: ""
            },
            {
                url: "img/screenshots/ff1.webp",
                description: ""
            },
            {
                url: "img/screenshots/ff2.webp",
                description: ""
            },
            {
                url: "img/screenshots/ff3.webp",
                description: ""
            }
        ]
},
 "Sienna": {
                name: "Sienna",
                description: "Fork of the fast-paced one-button platformer by SimonLarsen with a bunch of new features.",
                repoUrl: "https://codeberg.org/glitchapp/sienna-mobile",
                   screenshots: [
            {
                url: "img/screenshots/sienna-highres.webp",
                description: ""
            },
            {
                url: "img/screenshots/sienna2.webp",
                description: ""
            },
            {
                url: "img/screenshots/sienna3.webp",
                description: ""
            },
            {
                url: "img/screenshots/sienna4.webp",
                description: ""
            }
        ]
            },
 "Mrrescue": {
                name: "Mrrescue",
                description: "Fork of Mr.Rescue by Simon Larsen with a lot of extra features.",
                repoUrl: "https://codeberg.org/glitchapp/mr-rescue-mobile",
                screenshots: [
            {
                url: "img/screenshots/mrrescue-highres.webp",
                description: ""
            },
            {
                url: "img/screenshots/mrrescue2.webp",
                description: ""
            },
            {
                url: "img/screenshots/mrrescue3.webp",
                description: ""
            },
            {
                url: "img/screenshots/mrrescue4.webp",
                description: ""
            }
        ]
            }
            // Add other games here with similar structure
        };

        openGameModal(gameData[gameName]);
          
          // Open modal or update UI with the selected game info and screenshot
        console.log(`Opening modal for ${gameName} with image ${highResImage}`);
    });
});
