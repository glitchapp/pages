// Minimize the window
function minimizeWindow(windowId, event) {
    event.stopPropagation(); // Stop event propagation to prevent interference with drag
    const window = document.getElementById(windowId);
    window.classList.add('minimized'); // Hide the window
}

// Close the window
function closeWindow(windowId, event) {
    event.stopPropagation(); // Stop event propagation to prevent interference with drag
    const window = document.getElementById(windowId);
    window.style.display = 'none'; // Hide the window completely
}

// Reopen the window
function reopenWindow(windowId) {
    const window = document.getElementById(windowId);
    window.style.display = 'block'; // Show the window again
    window.classList.remove('minimized'); // Remove minimized state
}


// Make the DIV element draggable
function dragElement(elmnt) {
    var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    if (document.getElementById(elmnt.id + "header")) {
        // if present, the header is where you move the DIV from:
        document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
    } else {
        // otherwise, move the DIV from anywhere inside the DIV:
        elmnt.onmousedown = dragMouseDown;
    }
    
   function dragMouseDown(e) {
        e = e || window.event;
        e.preventDefault();
        pos3 = e.clientX;
        pos4 = e.clientY;
        document.onmouseup = closeDragElement;
        document.onmousemove = elementDrag;

        // Disable drag functionality if clicking a minimize or close button
        const target = e.target || e.srcElement;
        if (target && (target.classList.contains('minimize-btn') || target.classList.contains('close-btn'))) {
            e.stopImmediatePropagation(); // Stop any drag behavior from being triggered.
            return;
        }
    }

     function elementDrag(e) {
        e = e || window.event;
        e.preventDefault();
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
        elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
    }

      function closeDragElement() {
        document.onmouseup = null;
        document.onmousemove = null;
    }
}
